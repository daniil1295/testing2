
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/27/2017 00:32:55
-- Generated from EDMX file: D:\projects\EntityDesigner\EntityDesigner\Library.edmx
-- --------------------------------------------------
CREATE DATABASE LibraryDB;
GO
SET QUOTED_IDENTIFIER OFF;
GO
USE [LibraryDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'BookSet'
CREATE TABLE [dbo].[BookSet] (
    [BookId] int IDENTITY(1,1) NOT NULL,
    [BookName] nvarchar(max)  NOT NULL,
    [BookPrice] float  NOT NULL,
    [BookYearOfWriting] nvarchar(max)  NOT NULL,
    [BookPagesCount] nvarchar(max)  NOT NULL,
    [LanguageId] int  NOT NULL,
    [BookPrinting] nvarchar(max)  NOT NULL,
    [PublishingHouseId] int  NOT NULL,
    [BookISBN] nvarchar(max)  NOT NULL,
    [BookSizeMillimeters] nvarchar(max)  NOT NULL,
    [CoverTypeId] int  NOT NULL,
    [BookCoverPicture] varbinary(max)  NULL
);
GO

-- Creating table 'AuthorSet'
CREATE TABLE [dbo].[AuthorSet] (
    [AuthorId] int IDENTITY(1,1) NOT NULL,
    [AuthorFirstName] nvarchar(max)  NOT NULL,
    [AuthorLastName] nvarchar(max)  NOT NULL,
    [AuthorPatronymic] nvarchar(max)  NOT NULL,
    [AuthorPenName] nvarchar(max)  NULL
);
GO

-- Creating table 'GenreSet'
CREATE TABLE [dbo].[GenreSet] (
    [GenreId] int IDENTITY(1,1) NOT NULL,
    [GenreName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenreToBookSet'
CREATE TABLE [dbo].[GenreToBookSet] (
    [GenreToBookId] int IDENTITY(1,1) NOT NULL,
    [GenreId] int  NOT NULL,
    [BookId] int  NOT NULL
);
GO

-- Creating table 'AuthorToBookSet'
CREATE TABLE [dbo].[AuthorToBookSet] (
    [AuthorToBookId] int IDENTITY(1,1) NOT NULL,
    [AuthorId] int  NOT NULL,
    [BookId] int  NOT NULL
);
GO

-- Creating table 'LanguageSet'
CREATE TABLE [dbo].[LanguageSet] (
    [LanguageId] int IDENTITY(1,1) NOT NULL,
    [LanguageName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PublishingHouseSet'
CREATE TABLE [dbo].[PublishingHouseSet] (
    [PublishingHouseId] int IDENTITY(1,1) NOT NULL,
    [PublishingHouseName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CoverTypeSet'
CREATE TABLE [dbo].[CoverTypeSet] (
    [CoverTypeId] int IDENTITY(1,1) NOT NULL,
    [CoverTypeName] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [BookId] in table 'BookSet'
ALTER TABLE [dbo].[BookSet]
ADD CONSTRAINT [PK_BookSet]
    PRIMARY KEY CLUSTERED ([BookId] ASC);
GO

-- Creating primary key on [AuthorId] in table 'AuthorSet'
ALTER TABLE [dbo].[AuthorSet]
ADD CONSTRAINT [PK_AuthorSet]
    PRIMARY KEY CLUSTERED ([AuthorId] ASC);
GO

-- Creating primary key on [GenreId] in table 'GenreSet'
ALTER TABLE [dbo].[GenreSet]
ADD CONSTRAINT [PK_GenreSet]
    PRIMARY KEY CLUSTERED ([GenreId] ASC);
GO

-- Creating primary key on [GenreToBookId] in table 'GenreToBookSet'
ALTER TABLE [dbo].[GenreToBookSet]
ADD CONSTRAINT [PK_GenreToBookSet]
    PRIMARY KEY CLUSTERED ([GenreToBookId] ASC);
GO

-- Creating primary key on [AuthorToBookId] in table 'AuthorToBookSet'
ALTER TABLE [dbo].[AuthorToBookSet]
ADD CONSTRAINT [PK_AuthorToBookSet]
    PRIMARY KEY CLUSTERED ([AuthorToBookId] ASC);
GO

-- Creating primary key on [LanguageId] in table 'LanguageSet'
ALTER TABLE [dbo].[LanguageSet]
ADD CONSTRAINT [PK_LanguageSet]
    PRIMARY KEY CLUSTERED ([LanguageId] ASC);
GO

-- Creating primary key on [PublishingHouseId] in table 'PublishingHouseSet'
ALTER TABLE [dbo].[PublishingHouseSet]
ADD CONSTRAINT [PK_PublishingHouseSet]
    PRIMARY KEY CLUSTERED ([PublishingHouseId] ASC);
GO

-- Creating primary key on [CoverTypeId] in table 'CoverTypeSet'
ALTER TABLE [dbo].[CoverTypeSet]
ADD CONSTRAINT [PK_CoverTypeSet]
    PRIMARY KEY CLUSTERED ([CoverTypeId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AuthorId] in table 'AuthorToBookSet'
ALTER TABLE [dbo].[AuthorToBookSet]
ADD CONSTRAINT [FK_AuthorAuthorToBook]
    FOREIGN KEY ([AuthorId])
    REFERENCES [dbo].[AuthorSet]
        ([AuthorId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AuthorAuthorToBook'
CREATE INDEX [IX_FK_AuthorAuthorToBook]
ON [dbo].[AuthorToBookSet]
    ([AuthorId]);
GO

-- Creating foreign key on [BookId] in table 'AuthorToBookSet'
ALTER TABLE [dbo].[AuthorToBookSet]
ADD CONSTRAINT [FK_BookAuthorToBook]
    FOREIGN KEY ([BookId])
    REFERENCES [dbo].[BookSet]
        ([BookId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BookAuthorToBook'
CREATE INDEX [IX_FK_BookAuthorToBook]
ON [dbo].[AuthorToBookSet]
    ([BookId]);
GO

-- Creating foreign key on [BookId] in table 'GenreToBookSet'
ALTER TABLE [dbo].[GenreToBookSet]
ADD CONSTRAINT [FK_BookGenreToBook]
    FOREIGN KEY ([BookId])
    REFERENCES [dbo].[BookSet]
        ([BookId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BookGenreToBook'
CREATE INDEX [IX_FK_BookGenreToBook]
ON [dbo].[GenreToBookSet]
    ([BookId]);
GO

-- Creating foreign key on [GenreId] in table 'GenreToBookSet'
ALTER TABLE [dbo].[GenreToBookSet]
ADD CONSTRAINT [FK_GenreGenreToBook]
    FOREIGN KEY ([GenreId])
    REFERENCES [dbo].[GenreSet]
        ([GenreId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenreGenreToBook'
CREATE INDEX [IX_FK_GenreGenreToBook]
ON [dbo].[GenreToBookSet]
    ([GenreId]);
GO

-- Creating foreign key on [LanguageId] in table 'BookSet'
ALTER TABLE [dbo].[BookSet]
ADD CONSTRAINT [FK_LanguageBook]
    FOREIGN KEY ([LanguageId])
    REFERENCES [dbo].[LanguageSet]
        ([LanguageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LanguageBook'
CREATE INDEX [IX_FK_LanguageBook]
ON [dbo].[BookSet]
    ([LanguageId]);
GO

-- Creating foreign key on [PublishingHouseId] in table 'BookSet'
ALTER TABLE [dbo].[BookSet]
ADD CONSTRAINT [FK_PublishingHouseBook]
    FOREIGN KEY ([PublishingHouseId])
    REFERENCES [dbo].[PublishingHouseSet]
        ([PublishingHouseId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PublishingHouseBook'
CREATE INDEX [IX_FK_PublishingHouseBook]
ON [dbo].[BookSet]
    ([PublishingHouseId]);
GO

-- Creating foreign key on [CoverTypeId] in table 'BookSet'
ALTER TABLE [dbo].[BookSet]
ADD CONSTRAINT [FK_CoverTypeBook]
    FOREIGN KEY ([CoverTypeId])
    REFERENCES [dbo].[CoverTypeSet]
        ([CoverTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CoverTypeBook'
CREATE INDEX [IX_FK_CoverTypeBook]
ON [dbo].[BookSet]
    ([CoverTypeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------