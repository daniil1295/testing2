﻿using Library.Models;
using Library.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Library.XAML
{
    /// <summary>
    /// Interaction logic for SearchAuthorWindow.xaml
    /// </summary>
    public partial class SearchAuthorWindow : Window
    {
        private AuthorRepository _auhtorRepository = new AuthorRepository();
        private ShortAuthorInfo selectedAuthor;
        public SearchAuthorWindow()
        {
            InitializeComponent();
            RefreshMainTable();
        }
        public ShortAuthorInfo GetSelectedAuthor()
        {
            return selectedAuthor;
        }
        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAddAuthors.SelectedItem != null)
            {
                selectedAuthor = _auhtorRepository.GetShortAuthorInfoById(((AuthorsSet)dataGridAddAuthors.SelectedItem).AuthorId);
                this.DialogResult = true;
                return;
            }
            MessageBox.Show("Author are not selected");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public void RefreshMainTable()
        {
            dataGridAddAuthors.ItemsSource = _auhtorRepository.GetAllAuthors();
        }

        private void btnSearchByTextBox_Click(object sender, RoutedEventArgs e)
        {
            dataGridAddAuthors.ItemsSource = _auhtorRepository.GetAuthorsWithCondition(tbConditionString.Text);
        }

        private void btnRefreshTable_Click(object sender, RoutedEventArgs e)
        {
            RefreshMainTable();
        }
    }
}
