﻿using Library.Models.Enums;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Library.XAML
{
    /// <summary>
    /// Interaction logic for TakeWindow.xaml
    /// </summary>
    public partial class TakeWindow : Window
    {
        JournalNoteRepository _journalNoreRepository = new JournalNoteRepository();
        private int _bookId;
        public TakeWindow(int bookId)
        {
            InitializeComponent();
            _bookId = bookId;
        }
        private bool IsValidInfo()
        {
            if(tbTakeBookVisitorName.Text==""||tbTakeBookVisitorPhone.Text=="")
            {
                return false;
            }
            return true;
        }
        private void btnTakeBook_Click(object sender, RoutedEventArgs e)
        {
            if(!IsValidInfo())
            {
                MessageBox.Show("Invalid data");
                return;
            }
            _journalNoreRepository.CreateNewNote(tbTakeBookVisitorName.Text, tbTakeBookVisitorPhone.Text, _bookId, (int)JournalNoteTypes.Taken);
            this.DialogResult = true;
        }
    }
}
