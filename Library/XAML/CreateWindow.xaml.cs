﻿using Library.Helpers;
using Library.Models;
using Library.Repositories;
using Library.XAML;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Library
{
    /// <summary>
    /// Interaction logic for AdminPanel.xaml
    /// </summary>
    public partial class CreateWindow : Window
    {
        private ImageHelper _imageHelper = new ImageHelper();
        private BookRepository _bookRepository = new BookRepository();
        private LanguageRepository _languageRepository = new LanguageRepository();
        private PublishingHouseRepository _publishingHouseRepository = new PublishingHouseRepository();
        private CoverTypeRepository _coverTypeRepository = new CoverTypeRepository();
        private AuthorRepository _authorRepository = new AuthorRepository();
        private GenreRepository _genreRepository = new GenreRepository();
        private GenreToBookRepository _genreToBookRepository = new GenreToBookRepository();
        private AuthorToBookRepository _authorToBookRepository = new AuthorToBookRepository();

        public CreateWindow()
        {
            InitializeComponent();
            comboCreateBookLanguage.ItemsSource = _languageRepository.GetAllLanguages();
            comboCreateBookPublishingHouse.ItemsSource = _publishingHouseRepository.GetAllPublishingHouses();
            comboCreateBookCoverType.ItemsSource = _coverTypeRepository.GetAllCoverTypes();
            comboCreateBookGenres.ItemsSource = _genreRepository.GetAllGenres();
        }
        private bool IsValidBookInfo()
        {
            double tmpDouble;
            int tmpInt;
            if (!Double.TryParse(tbCreateBookPrice.Text, out tmpDouble) || !Int32.TryParse(tbCreateBookPages.Text, out tmpInt) || !Int32.TryParse(tbCreateBookYear.Text, out tmpInt) || !Int32.TryParse(tbCreateBookPrinting.Text, out tmpInt))
            {
                return false;
            }
            if (listBoxCreateBookAuthors.Items.Count == 0 || listBoxCreateBookGenres.Items.Count == 0)
            {
                return false;
            }
            if (Convert.ToInt32(tbCreateBookPages.Text) < 0 || Convert.ToInt32(tbCreateBookYear.Text) < 0 || Convert.ToInt32(tbCreateBookPrinting.Text) < 0 || Convert.ToDouble(tbCreateBookPrice.Text) < 0)
            {
                return false;
            }
            if (imageCover.Source == null)
            {
                return false;
            }
            return true;
        }
        private void btnCreateBook_Click(object sender, RoutedEventArgs e)
        {
            if (!IsValidBookInfo())
            {
                MessageBox.Show("Invalid data");
                return;
            }
            var authorsId = new List<int>();
            var genresId = new List<int>();
            for (var index = 0; index < listBoxCreateBookAuthors.Items.Count; index++)
            {
                authorsId.Add(((ShortAuthorInfo)listBoxCreateBookAuthors.Items[index]).AuthorId);
            }
            for (var index = 0; index < listBoxCreateBookGenres.Items.Count; index++)
            {
                genresId.Add(((GenresSet)listBoxCreateBookGenres.Items[index]).GenreId);
            }
            var newBookId = _bookRepository.CreateNewBook(tbCreateBookISBN.Text, tbCreateBookName.Text, Convert.ToInt32(tbCreateBookPages.Text), Convert.ToDouble(tbCreateBookPrice.Text), Convert.ToInt32(tbCreateBookPrinting.Text), tbCreateBookSize.Text, Convert.ToInt32(tbCreateBookYear.Text), Convert.ToInt32(comboCreateBookCoverType.SelectedValue), Convert.ToInt32(comboCreateBookLanguage.SelectedValue), Convert.ToInt32(comboCreateBookPublishingHouse.SelectedValue), _imageHelper.BitmapToByteArray(imageCover.Source as BitmapImage));
            _authorToBookRepository.CreateNewAuthorToBookNote(authorsId, newBookId);
            _genreToBookRepository.CreateNewGenreToBookNote(genresId, newBookId);
            MessageBox.Show("Successfully added");
            ClearInputBoxes();
        }

        private void ClearInputBoxes()
        {
            tbCreateBookISBN.Text = "";
            tbCreateBookName.Text = "";
            tbCreateBookPages.Text = "";
            tbCreateBookPrice.Text = "";
            tbCreateBookPrinting.Text = "";
            tbCreateBookSize.Text = "";
            tbCreateBookYear.Text = "";
            listBoxCreateBookAuthors.Items.Clear();
            listBoxCreateBookGenres.Items.Clear();
            var selectedNullIndex = 0;
            comboCreateBookCoverType.SelectedIndex = selectedNullIndex;
            comboCreateBookGenres.SelectedIndex = selectedNullIndex;
            comboCreateBookLanguage.SelectedIndex = selectedNullIndex;
            comboCreateBookPublishingHouse.SelectedIndex = selectedNullIndex;
            imageCover.Source = null;
        }


        private void btnCreateBookAuthorsAdd_Click(object sender, RoutedEventArgs e)
        {
            var searchAuthorWindow = new SearchAuthorWindow();
            searchAuthorWindow.ShowDialog();
            if (searchAuthorWindow.DialogResult == false)
            {
                return;
            }
            var selectedAuthor = searchAuthorWindow.GetSelectedAuthor();
            if (!listBoxCreateBookAuthors.Items.Cast<ShortAuthorInfo>().Any(author=> author.AuthorId==selectedAuthor.AuthorId))
            {
                listBoxCreateBookAuthors.Items.Add(selectedAuthor);
            }
        }

        private void btnCreateBookAuthorsDelete_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxCreateBookAuthors.SelectedItem != null)
            {
                listBoxCreateBookAuthors.Items.RemoveAt(listBoxCreateBookAuthors.Items.IndexOf(listBoxCreateBookAuthors.SelectedItem));
            }
        }

        private void btnCreateBookGenreAdd_Click(object sender, RoutedEventArgs e)
        {
            var selectedGenre = (GenresSet)comboCreateBookGenres.SelectedItem;
            if(selectedGenre==null)
            {
                return;
            }
            if (!listBoxCreateBookGenres.Items.Cast<GenresSet>().Any(genre=> genre.GenreId== selectedGenre.GenreId))
            {
                listBoxCreateBookGenres.Items.Add(selectedGenre);
            }
        }

        private void btnCreateBookChoiceImage_Click(object sender, RoutedEventArgs e)
        {
            imageCover.Source = _imageHelper.OpenImage();
        }

        private void btnCreateBookGenreDelete_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxCreateBookGenres.SelectedItem != null)
            {
                listBoxCreateBookGenres.Items.RemoveAt(listBoxCreateBookGenres.Items.IndexOf(listBoxCreateBookGenres.SelectedItem));
            }
        }
    }
}

