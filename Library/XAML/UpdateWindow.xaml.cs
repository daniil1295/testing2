﻿using Library.Helpers;
using Library.Models;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Library.XAML
{
    /// <summary>
    /// Interaction logic for UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        private PublishingHouseRepository _publishingHouseRepository = new PublishingHouseRepository();
        private AuthorToBookRepository _authorToBookRepository = new AuthorToBookRepository();
        private GenreToBookRepository _genreToBookRepository = new GenreToBookRepository();
        private CoverTypeRepository _coverTypeRepository = new CoverTypeRepository();
        private LanguageRepository _languageRepository = new LanguageRepository();
        private GenreRepository _genreRepository = new GenreRepository();
        private BookRepository _bookRepository = new BookRepository();
        private ImageHelper _imageHelper = new ImageHelper();
        private InfoHelper _infoHelper = new InfoHelper();
        private List<int> _authorsIdOld = new List<int>();
        private List<int> _genresIdOld = new List<int>();
        private List<int> _authorsIdCurrent = new List<int>();
        private List<int> _genresIdCurrent = new List<int>();
        public BookSet BookForUpdate { get; set; }

        public UpdateWindow(BookSet bookForUpdate)
        {
            InitializeComponent();
            BookForUpdate = bookForUpdate;
            LoadInfoFromBook();
        }

        private void LoadInfoFromBook()
        {
            tbUpdateBookISBN.Text = BookForUpdate.BookISBN;
            tbUpdateBookName.Text = BookForUpdate.BookName;
            tbUpdateBookPages.Text = BookForUpdate.BookPagesCount.ToString();
            tbUpdateBookPrice.Text = BookForUpdate.BookPrice.ToString();
            tbUpdateBookPrinting.Text = BookForUpdate.BookPrinting.ToString();
            tbUpdateBookSize.Text = BookForUpdate.BookSizeMillimeters;
            tbUpdateBookYear.Text = BookForUpdate.BookYearOfWriting.ToString();
            comboUpdateBookLanguage.ItemsSource = _languageRepository.GetAllLanguages();
            comboUpdateBookPublishingHouse.ItemsSource = _publishingHouseRepository.GetAllPublishingHouses();
            comboUpdateBookCoverType.ItemsSource = _coverTypeRepository.GetAllCoverTypes();
            comboUpdateBookGenres.ItemsSource = _genreRepository.GetAllGenres();
            imageCover.Source = _imageHelper.ByteArrayToImageSource(BookForUpdate.BookCoverPicture);
            var genresList = _infoHelper.GetGenresInModelFormat(BookForUpdate.GenreToBookSet);
            for (var index = 0;index<genresList.Count;index++)
            {
                listBoxUpdateBookGenres.Items.Add(genresList[index]);
                _genresIdOld.Add(genresList[index].GenreId);
            }
            var authorsList = _infoHelper.GetAuthorsInModelFormat(BookForUpdate.AuthorToBookSet);
            for (var index = 0; index < authorsList.Count; index++)
            {
                listBoxUpdateBookAuthors.Items.Add(authorsList[index]);
                _authorsIdOld.Add(authorsList[index].AuthorId);
            }
            comboUpdateBookCoverType.SelectedItem = comboUpdateBookCoverType.Items.Cast<CoverTypesSet>().Where(coverType=> coverType.CoverTypeId== BookForUpdate.CoverTypesSet.CoverTypeId).FirstOrDefault();
            comboUpdateBookLanguage.SelectedItem = comboUpdateBookLanguage.Items.Cast<LanguagesSet>().Where(language => language.LanguageId == BookForUpdate.LanguagesSet.LanguageId).FirstOrDefault();
            comboUpdateBookPublishingHouse.SelectedItem = comboUpdateBookPublishingHouse.Items.Cast<PublishingHousesSet>().Where(publishingHouse => publishingHouse.PublishingHouseId == BookForUpdate.PublishingHousesSet.PublishingHouseId).FirstOrDefault();
        }

        private bool IsValidBookInfo()
        {
            double tmpDouble;
            int tmpInt;
            if (!Double.TryParse(tbUpdateBookPrice.Text, out tmpDouble) || !Int32.TryParse(tbUpdateBookPages.Text, out tmpInt) || !Int32.TryParse(tbUpdateBookYear.Text, out tmpInt) || !Int32.TryParse(tbUpdateBookPrinting.Text, out tmpInt))
            {
                return false;
            }
            if (listBoxUpdateBookAuthors.Items.Count == 0 || listBoxUpdateBookGenres.Items.Count == 0)
            {
                return false;
            }
            if (Convert.ToInt32(tbUpdateBookPages.Text) < 0 || Convert.ToInt32(tbUpdateBookYear.Text) < 0 || Convert.ToInt32(tbUpdateBookPrinting.Text) < 0 || Convert.ToDouble(tbUpdateBookPrice.Text) < 0)
            {
                return false;
            }
            if (imageCover.Source == null)
            {
                return false;
            }
            return true;
        }

        private void btnUpdateBook_Click(object sender, RoutedEventArgs e)
        {
            if (!IsValidBookInfo())
            {
                MessageBox.Show("Invalid data");
                return;
            }
            _bookRepository.UpdateBook(tbUpdateBookISBN.Text, tbUpdateBookName.Text, Convert.ToInt32(tbUpdateBookPages.Text), Convert.ToDouble(tbUpdateBookPrice.Text), Convert.ToInt32(tbUpdateBookPrinting.Text), tbUpdateBookSize.Text, Convert.ToInt32(tbUpdateBookYear.Text), Convert.ToInt32(comboUpdateBookCoverType.SelectedValue), Convert.ToInt32(comboUpdateBookLanguage.SelectedValue), Convert.ToInt32(comboUpdateBookPublishingHouse.SelectedValue), _imageHelper.BitmapToByteArray(imageCover.Source as BitmapImage), BookForUpdate.BookId);
            FillCurrentAuthorsId();
            FillCurrentGenresId();
            var genresIdToRemove = GetGenresToRemove();
            var genresIdToCreate = GetGenresToCreate();
            var authorsIdToRemove = GetAuthorsToRemove();
            var authorsIdToCreate = GetAuthorsToCreate();
            if(genresIdToRemove.Count!=0)
            {
                _genreToBookRepository.RemoveGenreToBookNote(genresIdToRemove, BookForUpdate.BookId);
            }
            if (genresIdToCreate.Count != 0)
            {
                _genreToBookRepository.CreateNewGenreToBookNote(genresIdToCreate, BookForUpdate.BookId);
            }
            if (authorsIdToRemove.Count != 0)
            {
                _authorToBookRepository.RemoveAuthorToBookNote(authorsIdToRemove, BookForUpdate.BookId);
            }
            if (authorsIdToCreate.Count != 0)
            {
                _authorToBookRepository.CreateNewAuthorToBookNote(authorsIdToCreate, BookForUpdate.BookId);
            }
            this.DialogResult = true;
        }

        private void FillCurrentGenresId()
        {
            for (var index = 0; index < listBoxUpdateBookGenres.Items.Count; index++)
            {
                _genresIdCurrent.Add(((GenresSet)listBoxUpdateBookGenres.Items[index]).GenreId);
            }
        }
        private void FillCurrentAuthorsId()
        {
            for (var index = 0; index < listBoxUpdateBookAuthors.Items.Count; index++)
            {
                _authorsIdCurrent.Add(((ShortAuthorInfo)listBoxUpdateBookAuthors.Items[index]).AuthorId);
            }
        }
        private List<int> GetGenresToRemove()
        {
            var genresIdToRemove = new List<int>();
            //find genreToBook connection which need to remove
            for (var index = 0; index < _genresIdOld.Count; index++)
            {
                if (!_genresIdCurrent.Contains(_genresIdOld[index]))
                {
                    genresIdToRemove.Add(_genresIdOld[index]);
                }
            }
            return genresIdToRemove;
        }

        private List<int> GetGenresToCreate()
        {
            var genresIdToCreate = new List<int>();
            //find genreToBook connection which need to create
            for (var index = 0; index < _genresIdCurrent.Count; index++)
            {
                if (!_genresIdOld.Contains(_genresIdCurrent[index]))
                {
                    genresIdToCreate.Add(_genresIdCurrent[index]);
                }
            }
            return genresIdToCreate;
        }

        private List<int> GetAuthorsToRemove()
        {
            var authorsIdToRemove = new List<int>();
            //find authorToBook connection which need to remove
            for (var index = 0; index < _authorsIdOld.Count; index++)
            {
                if (!_authorsIdCurrent.Contains(_authorsIdOld[index]))
                {
                    authorsIdToRemove.Add(_authorsIdOld[index]);
                }
            }
            return authorsIdToRemove;
        }

        private List<int> GetAuthorsToCreate()
        {
            var authorsIdToCreate = new List<int>();
            //find authorToBook connection which need to create
            for (var index = 0; index < _authorsIdCurrent.Count; index++)
            {
                if (!_authorsIdOld.Contains(_authorsIdCurrent[index]))
                {
                    authorsIdToCreate.Add(_authorsIdCurrent[index]);
                }
            }
            return authorsIdToCreate;
        }

        private void btnUpdateBookAuthorsAdd_Click(object sender, RoutedEventArgs e)
        {
            var searchAuthorWindow = new SearchAuthorWindow();
            searchAuthorWindow.ShowDialog();
            if (searchAuthorWindow.DialogResult == false)
            {
                return;
            }
            var selectedAuthor = searchAuthorWindow.GetSelectedAuthor();
            if (!listBoxUpdateBookAuthors.Items.Cast<ShortAuthorInfo>().Any(author => author.AuthorId == selectedAuthor.AuthorId))
            {
                listBoxUpdateBookAuthors.Items.Add(selectedAuthor);
            }
        }

        private void btnUpdateBookAuthorsDelete_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxUpdateBookAuthors.SelectedItem != null)
            {
                listBoxUpdateBookAuthors.Items.RemoveAt(listBoxUpdateBookAuthors.Items.IndexOf(listBoxUpdateBookAuthors.SelectedItem));
            }
        }

        private void btnUpdateBookGenreAdd_Click(object sender, RoutedEventArgs e)
        {
            var selectedGenre = (GenresSet)comboUpdateBookGenres.SelectedItem;
            if (selectedGenre == null)
            {
                return;
            }
            if (!listBoxUpdateBookGenres.Items.Cast<GenresSet>().Any(genre => genre.GenreId == selectedGenre.GenreId))
            {
                listBoxUpdateBookGenres.Items.Add(selectedGenre);
            }
        }

        private void btnUpdateBookGenreDelete_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxUpdateBookGenres.SelectedItem != null)
            {
                listBoxUpdateBookGenres.Items.RemoveAt(listBoxUpdateBookGenres.Items.IndexOf(listBoxUpdateBookGenres.SelectedItem));
            }
        }

        private void btnUpdateBookChoiceImage_Click(object sender, RoutedEventArgs e)
        {
            imageCover.Source = _imageHelper.OpenImage();
        }
    }
}
