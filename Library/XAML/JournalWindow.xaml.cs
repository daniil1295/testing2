﻿using Library.Models;
using Library.Models.Enums;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Library.XAML
{
    /// <summary>
    /// Interaction logic for JournalWindow.xaml
    /// </summary>
    public partial class JournalWindow : Window
    {
        JournalNoteRepository _journalNoteRepository = new JournalNoteRepository();
        InfoHelper _infoHelper = new InfoHelper();
        public JournalWindow()
        {
            InitializeComponent();
            UpdateMainTable();


        }
        private void UpdateMainTable()
        {
            dataGridAllJournalNotes.ItemsSource = _infoHelper.GetShortJournalNoteInfo(_journalNoteRepository.GetAllJournalNotes());
        }

        private void btnChangeState_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAllJournalNotes.SelectedItem == null)
            {
                MessageBox.Show("Please, select book which was returned");
                return;
            }
            var selectedShortJournalNote = (ShortJournalNoteInfo)dataGridAllJournalNotes.SelectedItem;
            if(selectedShortJournalNote.NoteType==JournalNoteTypes.Returned.ToString())
            {
                MessageBox.Show("This book already returned");
                return;
            }
            _journalNoteRepository.SetJournalNoteTypeReturned(selectedShortJournalNote.JournalNoteId, (int)JournalNoteTypes.Returned, DateTime.Now);
            UpdateMainTable();
        }
    }
}
