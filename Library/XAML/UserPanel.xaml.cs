﻿using Library.Models;
using Library.Repositories;
using Library.XAML;
using System;
using System.Windows;


namespace Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 

        private BookRepository _bookRepository = new BookRepository();
        private AuthorToBookRepository _authorToBookRepository = new AuthorToBookRepository();
        private GenreToBookRepository _genreToBookRepository = new GenreToBookRepository();
        private JournalNoteRepository _journalNoteRepository = new JournalNoteRepository();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateMainTable();
        }
        private void UpdateMainTable()
        {
            dataGridAllBooks.ItemsSource = _bookRepository.GetAllBooks();
        }
        private void MenuItemCreate_Click(object sender, RoutedEventArgs e)
        {
            var createWindow = new CreateWindow();
            createWindow.ShowDialog();
            UpdateMainTable();
        }
        private void MenuItemUpdate_Click(object sender, RoutedEventArgs e)
        {
            if(dataGridAllBooks.SelectedItem==null)
            {
                MessageBox.Show("Please, select book which you need update");
                return;
            }
            var updateWindow = new UpdateWindow(_bookRepository.GetBookById(((AllBookInfo)dataGridAllBooks.SelectedItem).BookId));
            updateWindow.ShowDialog();
            UpdateMainTable();
        }
        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAllBooks.SelectedItem == null)
            {
                MessageBox.Show("Please, select book which you need remove");
                return;
            }
            var bookId = ((AllBookInfo)dataGridAllBooks.SelectedItem).BookId;
            _genreToBookRepository.RemoveGenreToBookNote(bookId);
            _authorToBookRepository.RemoveAuthorToBookNote(bookId);
            _journalNoteRepository.RemoveJournalNotesByBookId(bookId);
            _bookRepository.RemoveBook(bookId);
            MessageBox.Show("Success");
            UpdateMainTable();
        }

        private void MenuItemTake_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAllBooks.SelectedItem == null)
            {
                MessageBox.Show("Please, select book which you want take");
                return;
            }
            var bookId = ((AllBookInfo)dataGridAllBooks.SelectedItem).BookId;
            var takeWindow = new TakeWindow(bookId);
            takeWindow.ShowDialog();

        }

        private void MenuItemOpenJournal_Click(object sender, RoutedEventArgs e)
        {
            var journalWindow = new JournalWindow();
            journalWindow.ShowDialog();
        }
    }
}
