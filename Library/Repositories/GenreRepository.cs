﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class GenreRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public List<GenresSet> GetAllGenres()
        {
            var allGenres = new List<GenresSet>();
            foreach(var genre in _libraryDatabase.GenresSet)
            {
                allGenres.Add(genre);
            }
            return allGenres;
        }
    }
}
