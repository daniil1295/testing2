﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class GenreToBookRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public void CreateNewGenreToBookNote(List<int> genresIdList, int bookId)
        {
            for (var index = 0; index < genresIdList.Count; index++)
            {
                var genreToBook = new GenreToBookSet() { GenreId = genresIdList[index], BookId = bookId };
                _libraryDatabase.GenreToBookSet.Add(genreToBook);
            }
            _libraryDatabase.SaveChanges();
        }

        public void RemoveGenreToBookNote(List<int> genresIdList, int bookId)
        {
            for (var index = 0; index < genresIdList.Count; index++)
            {
                var genreId = genresIdList[index];
                var genreToBookSet = _libraryDatabase.GenreToBookSet.Where(genreToBook => genreToBook.GenreId == genreId && genreToBook.BookId == bookId).FirstOrDefault();
                _libraryDatabase.GenreToBookSet.Remove(genreToBookSet);
            }
            _libraryDatabase.SaveChanges();
        }

        public void RemoveGenreToBookNote(int bookId)
        {
            var notesToRemove = _libraryDatabase.GenreToBookSet.Where(genreToBook=> genreToBook.BookId==bookId);
            foreach(var note in notesToRemove)
            {
                _libraryDatabase.GenreToBookSet.Remove(note);
            }
            _libraryDatabase.SaveChanges();
        }
    }
}
