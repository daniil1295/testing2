﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class AuthorRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public List<AuthorsSet> GetAllAuthors()
        {
            var allAuthors = new List<AuthorsSet>();
            foreach(var author in _libraryDatabase.AuthorsSet)
            {
                allAuthors.Add(author);
            }
            return allAuthors;
        }
        public List<AuthorsSet> GetAuthorsWithCondition(string conditionString)
        {
            var allAuthorsWithCondition = new List<AuthorsSet>();
            foreach (var author in _libraryDatabase.AuthorsSet.Where(author=> author.AuthorLastName.Contains(conditionString)|| author.AuthorFirstName.Contains(conditionString) || author.AuthorPatronymic.Contains(conditionString) || author.AuthorPenName.Contains(conditionString)  ))
            {
                allAuthorsWithCondition.Add(author);
            }
            return allAuthorsWithCondition;
        }
        public ShortAuthorInfo GetShortAuthorInfoById(int authorId)
        {
            var neededAuthor = _libraryDatabase.AuthorsSet.Find(authorId);
            return new ShortAuthorInfo() { AuthorId = neededAuthor.AuthorId, AuthorFullName = neededAuthor.AuthorFirstName + " " + neededAuthor.AuthorLastName + " " + neededAuthor.AuthorPatronymic + " " + neededAuthor.AuthorPenName };
        }
    }
}
