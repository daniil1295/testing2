﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class JournalNoteRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public void CreateNewNote(string visitorName, string visitorPhone, int bookId, int noteTypeId)
        {
            var newNote = new JournalNotesSet();
            newNote.JournalNoteVisitorName = visitorName;
            newNote.JournalNoteVisitorPhone = visitorPhone;
            newNote.BookId = bookId;
            newNote.NoteTypeId = noteTypeId;
            newNote.JournalNoteTakenDate = DateTime.Now;
            _libraryDatabase.JournalNotesSet.Add(newNote);
            _libraryDatabase.SaveChanges();
        }
        public List<JournalNotesSet> GetAllJournalNotes()
        {
            var notes = new List<JournalNotesSet>();
            foreach(var note in _libraryDatabase.JournalNotesSet)
            {
                notes.Add(note);
            }
            return notes;
        }
        public void RemoveJournalNotesByBookId(int bookId)
        {
            var notesIdList = new List<int>();
            foreach(var note in _libraryDatabase.JournalNotesSet)
            {
                if(note.BookId==bookId)
                {
                    notesIdList.Add(note.JournalNoteId);
                }
            }
            for (var index = 0; index < notesIdList.Count; index++)
            {
                var noteId = notesIdList[index];
                var journalNotesSet = _libraryDatabase.JournalNotesSet.Where(journalNote=> journalNote.JournalNoteId == noteId ).FirstOrDefault();
                _libraryDatabase.JournalNotesSet.Remove(journalNotesSet);
            }
            _libraryDatabase.SaveChanges();
        }
        public void SetJournalNoteTypeReturned(int journalNoteId, int noteTypeId, DateTime returnDate)
        {
            var journalNotesSet = _libraryDatabase.JournalNotesSet.Find(journalNoteId);
            
            journalNotesSet.JournalNoteReturnedDate = returnDate;
            _libraryDatabase.SaveChanges();
            journalNotesSet.NoteTypeId = noteTypeId;
            _libraryDatabase.SaveChanges();

        }
    }
}
