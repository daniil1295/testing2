﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class AuthorToBookRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public void CreateNewAuthorToBookNote(List<int> authorsIdList, int bookId)
        {
            for(var index=0; index < authorsIdList.Count; index++)
            {
                var authorToBook = new AuthorToBookSet() { AuthorId = authorsIdList[index], BookId = bookId };
                _libraryDatabase.AuthorToBookSet.Add(authorToBook);
            }
            _libraryDatabase.SaveChanges();
        }
        public void RemoveAuthorToBookNote(List<int> authorsIdList, int bookId)
        {
            for (var index = 0; index < authorsIdList.Count; index++)
            {
                var authorId = authorsIdList[index];
                var authorToBookSet = _libraryDatabase.AuthorToBookSet.Where(authorToBook=> authorToBook.AuthorId==authorId && authorToBook.BookId == bookId).FirstOrDefault();
                _libraryDatabase.AuthorToBookSet.Remove(authorToBookSet);
            }
            _libraryDatabase.SaveChanges();
        }
        public void RemoveAuthorToBookNote(int bookId)
        {
            var notesToRemove = _libraryDatabase.AuthorToBookSet.Where(authorToBook => authorToBook.BookId == bookId);
            foreach (var note in notesToRemove)
            {
                _libraryDatabase.AuthorToBookSet.Remove(note);
            }
            _libraryDatabase.SaveChanges();
        }
    }
}
