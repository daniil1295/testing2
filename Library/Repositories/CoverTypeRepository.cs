﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Library.Repositories
{
    public class CoverTypeRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public List<CoverTypesSet> GetAllCoverTypes()
        {
            var allCoverTypes = new List<CoverTypesSet>();
            foreach (var coverType in _libraryDatabase.CoverTypesSet)
            {
                allCoverTypes.Add(coverType);
            }
            return allCoverTypes;
        }
    }
}
