﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Library.Repositories
{
    public class LanguageRepository 
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public List<LanguagesSet> GetAllLanguages()
        {
            var allLanguages = new List<LanguagesSet>();
            foreach (var language in _libraryDatabase.LanguagesSet)
            {
                allLanguages.Add(language);
            }
            return allLanguages;
        }
    }
}
