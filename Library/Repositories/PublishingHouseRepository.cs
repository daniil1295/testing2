﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Library.Repositories
{
    public class PublishingHouseRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public List<PublishingHousesSet> GetAllPublishingHouses()
        {
            var allPublishingHouses = new List<PublishingHousesSet>();
            foreach (var publishingHouse in _libraryDatabase.PublishingHousesSet)
            {
                allPublishingHouses.Add(publishingHouse);
            }
            return allPublishingHouses;
        }
    }
}
