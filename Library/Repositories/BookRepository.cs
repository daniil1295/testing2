﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Library
{
    public class BookRepository
    {
        private LibraryDatabase _libraryDatabase = new LibraryDatabase();
        private InfoHelper _infoHelper = new InfoHelper();

        public List<AllBookInfo> GetAllBooks()
        {
            _libraryDatabase = new LibraryDatabase();
            var allBooks = new List<AllBookInfo>();
            foreach(var book in _libraryDatabase.BookSet)
            {
                allBooks.Add(new AllBookInfo
                {
                    BookId = book.BookId,
                    BookISBN = book.BookISBN,
                    BookName = book.BookName,
                    BookPagesCount = book.BookPagesCount,
                    BookPrice = book.BookPrice,
                    BookPrinting = book.BookPrinting,
                    BookSizeMillimeters = book.BookSizeMillimeters,
                    BookYearOfWriting = book.BookYearOfWriting,
                    CoverTypeName = book.CoverTypesSet.CoverTypeName,
                    LanguageName = book.LanguagesSet.LanguageName,
                    BookAuthors = _infoHelper.GetAuthorsInStringFormat(book.AuthorToBookSet),
                    BookGenres = _infoHelper.GetGenresInStringFormat(book.GenreToBookSet),
                    PublishingHouseName = book.PublishingHousesSet.PublishingHouseName,
                    BookCoverPicture = book.BookCoverPicture
                });
            }
            return allBooks;
        }
        public int CreateNewBook(string bookISBN, string bookName, int bookPagesCount, double bookPrice, int bookPrinting, string bookSizeMillimeters, int bookYearOfWriting, int CoverTypeId, int LanguageId, int  PublishingHouseId, byte[] bookCoverPicture )
        {
            var newBook = new BookSet();
            newBook.BookCoverPicture = bookCoverPicture;
            newBook.BookISBN = bookISBN;
            newBook.BookName = bookName;
            newBook.BookPagesCount = bookPagesCount;
            newBook.BookPrice = bookPrice;
            newBook.BookPrinting = bookPrinting;
            newBook.BookSizeMillimeters = bookSizeMillimeters;
            newBook.BookYearOfWriting = bookYearOfWriting;
            newBook.CoverTypeId = CoverTypeId;
            newBook.LanguageId = LanguageId;
            newBook.PublishingHouseId = PublishingHouseId;
            _libraryDatabase.BookSet.Add(newBook);
            _libraryDatabase.SaveChanges();
            return newBook.BookId;
        }

        public void RemoveBook(int bookId)
        {
            _libraryDatabase = new LibraryDatabase();
            var bookForRemove = _libraryDatabase.BookSet.Find(bookId);
            _libraryDatabase.BookSet.Remove(bookForRemove);
            _libraryDatabase.SaveChanges();
        }

        public void UpdateBook(string bookISBN, string bookName, int bookPagesCount, double bookPrice, int bookPrinting, string bookSizeMillimeters, int bookYearOfWriting, int CoverTypeId, int LanguageId, int PublishingHouseId, byte[] bookCoverPicture, int bookId)
        {
            var bookForUpdate = _libraryDatabase.BookSet.Find(bookId);
            bookForUpdate.BookCoverPicture = bookCoverPicture;
            bookForUpdate.BookISBN = bookISBN;
            bookForUpdate.BookName = bookName;
            bookForUpdate.BookPagesCount = bookPagesCount;
            bookForUpdate.BookPrice = bookPrice;
            bookForUpdate.BookPrinting = bookPrinting;
            bookForUpdate.BookSizeMillimeters = bookSizeMillimeters;
            bookForUpdate.BookYearOfWriting = bookYearOfWriting;
            bookForUpdate.CoverTypeId = CoverTypeId;
            bookForUpdate.LanguageId = LanguageId;
            bookForUpdate.PublishingHouseId = PublishingHouseId;
            _libraryDatabase.SaveChanges();
            _libraryDatabase = new LibraryDatabase();
        }

        public BookSet GetBookById(int bookId)
        {
            return _libraryDatabase.BookSet.Find(bookId);
        }
    }
}
