﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Library.Helpers
{
    class ImageHelper
    {
        
        public ImageSource ByteArrayToImageSource(byte[] imageData)
        {
            var bitmapImage = new BitmapImage();
            using (var memoryStream = new MemoryStream(imageData))
            {

                bitmapImage.BeginInit();
                bitmapImage.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.UriSource = null;
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
            }
            return bitmapImage;
        }
        public byte[] BitmapToByteArray(BitmapImage bitmapImage)
        {
            byte[] imageData = null;
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (var memoryStream = new MemoryStream())
            {
                encoder.Save(memoryStream);
                imageData = memoryStream.ToArray();
            }
            return imageData;
        }
        public BitmapImage OpenImage()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName != "")
            {
                var standardImageWidth = 150;
                var standardImageHeight = 220;
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.UriSource = new Uri(openFileDialog.FileName);
                bitmapImage.DecodePixelWidth = standardImageWidth;
                bitmapImage.DecodePixelHeight = standardImageHeight;
                bitmapImage.EndInit();
                return bitmapImage;
            }
            return new BitmapImage();
        }
    }
}
