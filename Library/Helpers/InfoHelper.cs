﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class InfoHelper
    {
        public List<ShortJournalNoteInfo> GetShortJournalNoteInfo(List<JournalNotesSet> journalNotes)
        {
            var shortJournalNotes = new List<ShortJournalNoteInfo>();
            foreach(var journalNote in journalNotes)
            {
                shortJournalNotes.Add(new ShortJournalNoteInfo()
                {
                    JournalNoteId = journalNote.JournalNoteId,
                    JournalNoteTakenDate = journalNote.JournalNoteTakenDate,
                    JournalNoteReturnedDate = journalNote.JournalNoteReturnedDate,
                    JournalNoteVisitorName = journalNote.JournalNoteVisitorName,
                    JournalNoteVisitorPhone = journalNote.JournalNoteVisitorPhone,
                    NoteType = journalNote.NoteTypeSet.NoteTypeName,
                    ShortBookInfo = GetShortBookInfo(journalNote.BookSet)
                });
            }
            return shortJournalNotes;
        }

        private ShortBookInfo GetShortBookInfo(BookSet book)
        {
            return new ShortBookInfo() {BookAuthors = GetAuthorsInStringFormat(book.AuthorToBookSet), BookImage=book.BookCoverPicture, BookName=book.BookName, BookId=book.BookId };
        }
        public string GetAuthorsInStringFormat(ICollection<AuthorToBookSet> authorToBookSet)
        {
            var allAuthors = "";


            foreach (var authorToBook in authorToBookSet)
            {
                allAuthors += authorToBook.AuthorsSet.ToString() + ",\n";
            }
            var firstCharIndex = 0;
            var countOfCharsWhichNeedToCut = 2;
            if (allAuthors != "")
            {
                allAuthors = allAuthors.Substring(firstCharIndex, allAuthors.Length - countOfCharsWhichNeedToCut);
            }
            return allAuthors;
        }

        public string GetGenresInStringFormat(ICollection<GenreToBookSet> genreToBookSet)
        {
            var allGenres = "";
            foreach (var genreToBook in genreToBookSet)
            {
                allGenres += genreToBook.GenresSet.ToString();
            }
            var firstCharIndex = 0;
            var countOfCharsWhichNeedToCut = 2;
            if (allGenres != "")
            {
                allGenres = allGenres.Substring(firstCharIndex, allGenres.Length - countOfCharsWhichNeedToCut);
            }
            return allGenres;
        }

        public List<GenresSet> GetGenresInModelFormat(ICollection<GenreToBookSet> genreToBookSet)
        {
            var allGenres = new List<GenresSet>();
            foreach(var genreToBook in genreToBookSet)
            {
                allGenres.Add(genreToBook.GenresSet);
            }
            return allGenres;
        }
        public List<ShortAuthorInfo> GetAuthorsInModelFormat(ICollection<AuthorToBookSet> authorToBookSet)
        {
            var allAuthors = new List<ShortAuthorInfo>();
            foreach (var authorToBook in authorToBookSet)
            {
                allAuthors.Add(new ShortAuthorInfo() { AuthorFullName = authorToBook.AuthorsSet.ToString(), AuthorId= authorToBook.AuthorId});
            }
            return allAuthors;
        }

    }
}
