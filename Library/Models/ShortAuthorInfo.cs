﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class ShortAuthorInfo
    {
        public int AuthorId { get; set; }
        public string AuthorFullName { get; set; }
    }
}
