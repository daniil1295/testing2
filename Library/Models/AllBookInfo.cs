﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class AllBookInfo
    {
        public int BookId { get; set; }
        public string BookISBN { get; set; }
        public string BookName { get; set; }
        public int BookPagesCount { get; set; }
        public double BookPrice { get; set; }
        public int BookPrinting { get; set; }
        public string BookSizeMillimeters { get; set; }
        public int BookYearOfWriting { get; set; }
        public string CoverTypeName { get; set; }
        public string LanguageName { get; set; }
        public string BookAuthors { get; set; }
        public string BookGenres { get; set; }
        public string PublishingHouseName { get; set; }
        public byte[] BookCoverPicture { get; set; }

    }
}
