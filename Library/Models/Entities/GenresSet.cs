namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GenresSet")]
    public partial class GenresSet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GenresSet()
        {
            GenreToBookSet = new HashSet<GenreToBookSet>();
        }

        [Key]
        public int GenreId { get; set; }

        [Required]
        public string GenreName { get; set; }

        public override string ToString()
        {
            return GenreName + ",\n";
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenreToBookSet> GenreToBookSet { get; set; }
    }
}
