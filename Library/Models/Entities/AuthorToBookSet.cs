namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuthorToBookSet")]
    public partial class AuthorToBookSet
    {
        [Key]
        public int AuthorToBookId { get; set; }

        public int AuthorId { get; set; }

        public int BookId { get; set; }

        public virtual AuthorsSet AuthorsSet { get; set; }

        public virtual BookSet BookSet { get; set; }
    }
}
