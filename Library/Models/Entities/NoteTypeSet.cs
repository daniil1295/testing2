namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NoteTypeSet")]
    public partial class NoteTypeSet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NoteTypeSet()
        {
            JournalNotesSet = new HashSet<JournalNotesSet>();
        }

        [Key]
        public int NoteTypeId { get; set; }

        [Required]
        public string NoteTypeName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JournalNotesSet> JournalNotesSet { get; set; }
    }
}
