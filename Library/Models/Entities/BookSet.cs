namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BookSet")]
    public partial class BookSet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BookSet()
        {
            AuthorToBookSet = new HashSet<AuthorToBookSet>();
            GenreToBookSet = new HashSet<GenreToBookSet>();
            JournalNotesSet = new HashSet<JournalNotesSet>();
        }

        [Key]
        public int BookId { get; set; }

        [Required]
        public string BookName { get; set; }

        public double BookPrice { get; set; }

        public int BookYearOfWriting { get; set; }

        public int BookPagesCount { get; set; }

        public int LanguageId { get; set; }

        public int BookPrinting { get; set; }

        public int PublishingHouseId { get; set; }

        [Required]
        public string BookISBN { get; set; }

        [Required]
        public string BookSizeMillimeters { get; set; }

        public int CoverTypeId { get; set; }

        [Required]
        public byte[] BookCoverPicture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthorToBookSet> AuthorToBookSet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenreToBookSet> GenreToBookSet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JournalNotesSet> JournalNotesSet { get; set; }

        public virtual CoverTypesSet CoverTypesSet { get; set; }

        public virtual LanguagesSet LanguagesSet { get; set; }

        public virtual PublishingHousesSet PublishingHousesSet { get; set; }
    }
}
