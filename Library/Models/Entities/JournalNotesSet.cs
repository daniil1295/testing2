namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JournalNotesSet")]
    public partial class JournalNotesSet
    {
        [Key]
        public int JournalNoteId { get; set; }

        public int BookId { get; set; }

        public DateTime JournalNoteTakenDate { get; set; }

        public int NoteTypeId { get; set; }

        public string JournalNoteVisitorName { get; set; }

        public string JournalNoteVisitorPhone { get; set; }

        public DateTime? JournalNoteReturnedDate { get; set; }

        public virtual BookSet BookSet { get; set; }

        public virtual NoteTypeSet NoteTypeSet { get; set; }
    }
}
