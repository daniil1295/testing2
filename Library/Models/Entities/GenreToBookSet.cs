namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GenreToBookSet")]
    public partial class GenreToBookSet
    {
        [Key]
        public int GenreToBookId { get; set; }

        public int GenreId { get; set; }

        public int BookId { get; set; }

        public virtual BookSet BookSet { get; set; }

        public virtual GenresSet GenresSet { get; set; }
    }
}
