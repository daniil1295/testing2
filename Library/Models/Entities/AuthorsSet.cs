namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuthorsSet")]
    public partial class AuthorsSet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AuthorsSet()
        {
            AuthorToBookSet = new HashSet<AuthorToBookSet>();
        }

        [Key]
        public int AuthorId { get; set; }

        [Required]
        public string AuthorFirstName { get; set; }

        [Required]
        public string AuthorLastName { get; set; }

        public string AuthorPatronymic { get; set; }

        public string AuthorPenName { get; set; }

        public override string ToString()
        {
            return AuthorLastName + " " + AuthorFirstName + " " + AuthorPatronymic + " " + AuthorPenName;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthorToBookSet> AuthorToBookSet { get; set; }
    }
}
