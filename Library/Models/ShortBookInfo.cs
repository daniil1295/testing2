﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class ShortBookInfo
    {
        public string BookName { get; set; }
        public byte[] BookImage { get; set; }
        public string BookAuthors { get; set; }
        public int BookId { get; set; }
    }
}
