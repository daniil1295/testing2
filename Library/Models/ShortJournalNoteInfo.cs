﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class ShortJournalNoteInfo
    {
        public int JournalNoteId { get; set; }

        public DateTime JournalNoteTakenDate { get; set; }

        public string NoteType { get; set; }

        public string JournalNoteVisitorName { get; set; }

        public string JournalNoteVisitorPhone { get; set; }

        public DateTime? JournalNoteReturnedDate { get; set; }

        public virtual ShortBookInfo ShortBookInfo { get; set; }
    }
}
