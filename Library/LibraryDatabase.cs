namespace Library
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LibraryDatabase : DbContext
    {
        public LibraryDatabase()
            : base("name=LibraryDatabase")
        {
        }

        public virtual DbSet<AuthorsSet> AuthorsSet { get; set; }
        public virtual DbSet<AuthorToBookSet> AuthorToBookSet { get; set; }
        public virtual DbSet<BookSet> BookSet { get; set; }
        public virtual DbSet<CoverTypesSet> CoverTypesSet { get; set; }
        public virtual DbSet<GenresSet> GenresSet { get; set; }
        public virtual DbSet<GenreToBookSet> GenreToBookSet { get; set; }
        public virtual DbSet<JournalNotesSet> JournalNotesSet { get; set; }
        public virtual DbSet<LanguagesSet> LanguagesSet { get; set; }
        public virtual DbSet<NoteTypeSet> NoteTypeSet { get; set; }
        public virtual DbSet<PublishingHousesSet> PublishingHousesSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorsSet>()
                .HasMany(e => e.AuthorToBookSet)
                .WithRequired(e => e.AuthorsSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookSet>()
                .HasMany(e => e.AuthorToBookSet)
                .WithRequired(e => e.BookSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookSet>()
                .HasMany(e => e.GenreToBookSet)
                .WithRequired(e => e.BookSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookSet>()
                .HasMany(e => e.JournalNotesSet)
                .WithRequired(e => e.BookSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CoverTypesSet>()
                .HasMany(e => e.BookSet)
                .WithRequired(e => e.CoverTypesSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GenresSet>()
                .HasMany(e => e.GenreToBookSet)
                .WithRequired(e => e.GenresSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LanguagesSet>()
                .HasMany(e => e.BookSet)
                .WithRequired(e => e.LanguagesSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<NoteTypeSet>()
                .HasMany(e => e.JournalNotesSet)
                .WithRequired(e => e.NoteTypeSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PublishingHousesSet>()
                .HasMany(e => e.BookSet)
                .WithRequired(e => e.PublishingHousesSet)
                .WillCascadeOnDelete(false);
        }
    }
}
